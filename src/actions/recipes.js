import { ADD_RECIPE, TOGGLE_FAVORITE } from '../consts/action-types';

export const addRecipe = (title) => ({
  type: ADD_RECIPE,
  title: title.trim(),
  delayed: 1000
});

export const toggleFavorite = (id) => ({
  type: TOGGLE_FAVORITE,
  id,
  delayed: 1000
});
