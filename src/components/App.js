import React from 'react';
import Header from './Header';
import Recipes from './Recipes';
import AddRecipe from './AddRecipe';
import Counter from './Counter';
import Footer from './Footer';

const App = () => (
  <div>
    <Header>Recipe Book (<Counter />)</Header>

    <div className="main">

      <div className="pane">
        <Recipes />
      </div>

      <div className="pane">
        <AddRecipe />
      </div>

    </div>

    <Footer />
  </div>
);

export default App;