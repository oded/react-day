import React from 'react';
import classNames from 'classnames';

const Recipe = ({ recipe, toggleFavorite }) => {
  const classes = classNames(
    'recipe',
    { favorite: recipe.favorite }
  );

  return (
    <li className={ classes }
        onClick={ () => toggleFavorite(recipe.id) }>
      { recipe.title }
    </li>
  );
};

Recipe.propTypes = {
  recipe: React.PropTypes.object.isRequired,
  toggleFavorite: React.PropTypes.func.isRequired
};

export default Recipe;