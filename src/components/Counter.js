import React from 'react';
import { connect } from 'react-redux';

const Counter = ({ total }) => (
  <span>{ total }</span>
);

Counter.propTypes = {
  total: React.PropTypes.number.isRequired
};

////////

const mapStatToProps = state => ({
  total: state.recipes.length
});

export default connect(mapStatToProps)(Counter);