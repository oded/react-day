const logMiddleware = ({ getState, dispatch }) => next => action => {

  console.log(`Action: ${ action.type }`, action);

  if (action.delayed) {
    setTimeout(() => dispatch(action), action.delayed)
    action.delayed = false;
  } else {
    next(action);
  }

};

export default logMiddleware;
